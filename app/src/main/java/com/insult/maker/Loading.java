package com.insult.maker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;

import java.util.logging.Handler;

import com.insult.maker.InsultatorMain;

public class Loading extends Activity {
   private void callActivity() {
      this.startActivity((new Intent()).setClass(this, InsultatorMain.class));
      this.finish();
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      getWindow().addFlags(Window.FEATURE_NO_TITLE);
      this.setContentView(R.layout.loading);
      new android.os.Handler().postDelayed(new Runnable() {
         @Override
         public void run() {
            Loading.this.callActivity();
         }
      },3000L);

   }
}
