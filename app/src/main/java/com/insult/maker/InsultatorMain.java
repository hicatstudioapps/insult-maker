package com.insult.maker;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import java.io.File;
import java.util.HashMap;

import badabing.lib.ServerUtilities;
import com.insult.maker.InsultGenerator;
import com.insult.maker.SendForm;
import com.insult.maker.Settings;

public class InsultatorMain extends Activity implements OnInitListener {

   protected static final int MY_DATA_CHECK_CODE = 0;
   private static final String TAG = "DearSteveTAG";
   public static InsultGenerator generator;
   public static boolean is_male = true;
   private static int last_id = 0;
   private static String last_insult = "";
   public static TextToSpeech mTts;
   public static String name = "Steve";
   public static int selected_language = 0;
   public LinearLayout actions_container;
   private AdView adview;
   public ImageView female_button;
   private EditText friend_name_input;
   public ImageView info_button;
   SharedPreferences mPrefs;
   public ImageView male_button;
   public TextView repeat_button;
   public TextView save_button;
   public TextView share_button;
   private Button speakButton;

   private void alertFirstRun() {
      this.setRunned();
      Builder var1 = new Builder(this);
      var1.setMessage(this.getString(R.string.welcome_alert)).setCancelable(true).setPositiveButton(this.getString(R.string.shut_up), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.dismiss();
         }
      });
      var1.create().show();
   }

   private void firstRunPreferences() {
      this.mPrefs = this.getApplicationContext().getSharedPreferences("myAppPrefs", 0);
   }

   private void initialize_locale() {
      if(this.getResources().getConfiguration().locale.getLanguage().equals("es")) {
         selected_language = 1;
      } else {
         selected_language = 0;
      }

   }

   private void repeatInsult() {
      if(last_insult != null && last_insult != "") {
         mTts.speak(last_insult, 0, (HashMap)null);
      }

   }

   private void saveInsult() {
      if(last_insult != null && last_insult != "") {
         if(!"mounted".equals(Environment.getExternalStorageState())) {
            this.show_error_message(this.getString(R.string.error_sd_not_found));
         } else {
            File var2 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "media" + File.separator + "audio" + File.separator + "ringtones" + File.separator);
            if(!var2.exists() && !var2.mkdirs()) {
               this.show_error_message(this.getString(R.string.error_sd_not_found));
            } else {
               String var1;
               if(selected_language == 1) {
                  var1 = "Insulto_" + name + Integer.toString(last_id) + ".mp3";
               } else {
                  var1 = "Insult_" + name + Integer.toString(last_id) + ".mp3";
               }
               HashMap<String, String> hashMap = new HashMap<String, String>();
               hashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "file");
               var1 = var2.getAbsolutePath() + File.separator + var1;
               mTts.synthesizeToFile(last_insult, hashMap, var1);
               Toast.makeText(this, this.getString(R.string.save_succesful) + " " + var1, 1).show();
            }
         }
      }

   }

   public static void say_something_to_Steve(String var0) {
      if(var0 != null && generator != null) {
         last_insult = generator.get_insult(var0, selected_language);
         last_id = generator.get_last_id();
         mTts.speak(last_insult, 0, (HashMap)null);
      }

   }

   private void shareInsult() {
      if(last_insult != null && last_insult != "") {
         Intent var1 = new Intent("android.intent.action.SEND");
         var1.setType("text/plain");
         var1.putExtra("android.intent.extra.TEXT", last_insult.replaceAll("Stif", "Steve") + " " + this.getString(R.string.by_insultator)+" https://play.google.com/store/apps/details?id=com.insult.maker");

         try {
            this.startActivity(Intent.createChooser(var1, this.getString(R.string.share_title)));
         } catch (ActivityNotFoundException var2) {
            Builder var3 = new Builder(this);
            var3.setMessage(this.getString(R.string.share_error)).setCancelable(true).setPositiveButton(this.getString(R.string.share_error), new OnClickListener() {
               public void onClick(DialogInterface var1, int var2) {
                  var1.dismiss();
               }
            });
            var3.create().show();
         }
      }

   }

   private void show_error_message(String var1) {
      Builder var2 = new Builder(this);
      var2.setMessage(var1).setCancelable(true).setPositiveButton(this.getString(R.string.shut_up), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.dismiss();
         }
      });
      var2.create().show();
   }

   protected boolean checkTTS() {
      boolean var1 = true;
      if(mTts == null) {
         mTts = new TextToSpeech(this, this);
         if(mTts == null) {
            Builder var2 = new Builder(this);
            var2.setMessage(this.getString(R.string.no_TTS)).setCancelable(true).setNegativeButton(this.getString(R.string.shut_up), new OnClickListener() {
               public void onClick(DialogInterface var1, int var2) {
                  var1.dismiss();
               }
            }).setPositiveButton(this.getString(R.string.go_market), new OnClickListener() {
               public void onClick(DialogInterface var1, int var2) {
                  var1.dismiss();
                  Intent var3 = new Intent();
                  var3.setAction("android.speech.tts.engine.INSTALL_TTS_DATA");
                  InsultatorMain.this.startActivity(var3);
               }
            });
            var2.create().show();
            var1 = false;
         }
      }

      return var1;
   }

   public boolean getFirstRun() {
      return this.mPrefs.getBoolean("firstRun", true);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
       ServerUtilities.registerWithGCM(this);
      this.setContentView(R.layout.main);

      this.setVolumeControlStream(3);
      this.firstRunPreferences();
      this.adview = (AdView)this.findViewById(R.id.ad);
      AdRequest var2 = (new AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("TEST_DEVICE_ID").build();
      this.adview.loadAd(var2);
      this.initialize_locale();
      generator = new InsultGenerator();
      mTts = new TextToSpeech(this, this);
      this.actions_container = (LinearLayout)this.findViewById(R.id.actions_container);
      this.actions_container.setVisibility(4);
      this.speakButton = (Button)this.findViewById(R.id.speak_button);
      this.friend_name_input = (EditText)this.findViewById(R.id.friend);
      this.share_button = (TextView)this.findViewById(R.id.share_button);
      this.repeat_button = (TextView)this.findViewById(R.id.repeat_button);
      this.save_button = (TextView)this.findViewById(R.id.save_button);
      this.info_button = (ImageView)this.findViewById(R.id.info_button);
       //tyoe face
       ((TextView)findViewById(R.id.textView)).setTypeface(SpotTheCatApp.face);
       ((TextView)findViewById(R.id.textView2)).setTypeface(SpotTheCatApp.face);
       ((TextView)findViewById(R.id.friends_name)).setTypeface(SpotTheCatApp.faceR);
       this.speakButton.setTypeface(SpotTheCatApp.faceR);
      this.speakButton.setOnClickListener(new android.view.View.OnClickListener() {
          public void onClick(View var1) {
              ((InputMethodManager) InsultatorMain.this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
              if (InsultatorMain.this.checkTTS()) {
                  InsultatorMain.name = InsultatorMain.this.friend_name_input.getText().toString();
                  InsultatorMain.say_something_to_Steve(InsultatorMain.name);
                  InsultatorMain.this.actions_container.setVisibility(0);
              }

          }
      });
      this.male_button = (ImageView)this.findViewById(R.id.male_button);
      this.female_button = (ImageView)this.findViewById(R.id.female_button);
      is_male = true;
      this.male_button.setAlpha(255);
      this.female_button.setAlpha(90);
      this.male_button.setOnClickListener(new android.view.View.OnClickListener() {
          public void onClick(View var1) {
              InsultatorMain.is_male = true;
              InsultatorMain.this.male_button.setAlpha(255);
              InsultatorMain.this.female_button.setAlpha(90);
              InsultatorMain.generator.setInsults(InsultatorMain.selected_language, InsultatorMain.is_male);
              ((InputMethodManager) InsultatorMain.this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
          }
      });
      this.info_button.setOnClickListener(new android.view.View.OnClickListener() {
          public void onClick(View var1) {
              Intent var2 = (new Intent("android.intent.action.VIEW")).setData(Uri.parse(InsultatorMain.this.getString(R.string.faq_url)));
              InsultatorMain.this.startActivity(var2);
              SpotTheCatApp.showInterstitial();
          }
      });
      this.female_button.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            InsultatorMain.is_male = false;
            InsultatorMain.this.female_button.setAlpha(255);
            InsultatorMain.this.male_button.setAlpha(90);
            InsultatorMain.generator.setInsults(InsultatorMain.selected_language, InsultatorMain.is_male);
            ((InputMethodManager)InsultatorMain.this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
         }
      });
       this.share_button.setTypeface(SpotTheCatApp.faceR);
      this.share_button.setOnClickListener(new android.view.View.OnClickListener() {
          public void onClick(View var1) {
              InsultatorMain.this.shareInsult();
              ((InputMethodManager) InsultatorMain.this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
          }
      });
       this.repeat_button.setTypeface(SpotTheCatApp.faceR);
      this.repeat_button.setOnClickListener(new android.view.View.OnClickListener() {
          public void onClick(View var1) {
              InsultatorMain.this.repeatInsult();
              ((InputMethodManager) InsultatorMain.this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
          }
      });
       this.save_button.setTypeface(SpotTheCatApp.faceR);
      this.save_button.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            SpotTheCatApp.showInterstitial();
            InsultatorMain.this.saveInsult();
            ((InputMethodManager)InsultatorMain.this.getSystemService("input_method")).hideSoftInputFromWindow(var1.getWindowToken(), 0);
         }
      });
      if(this.getFirstRun()) {
       //  this.alertFirstRun();
      }
      findViewById(R.id.ib_menu).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            startActivity((new Intent()).setClass(getApplicationContext(), Settings.class));
         }
      });
      findViewById(R.id.imageButton2).setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {

            finish();
            SpotTheCatApp.showInterstitial();
         }
      });
      SpotTheCatApp.showInterstitial();
   }

   public boolean onCreateOptionsMenu(Menu var1) {
    //  this.getMenuInflater().inflate(R.menu.title_only, var1);
      return true;
   }

   public void onDestroy() {
      if(mTts != null) {
         mTts.stop();
         mTts.shutdown();
      }

      this.adview.destroy();
      super.onDestroy();
   }

   public void onInit(int var1) {
      if(var1 == 0) {
         if(-1 != 0 && -2 != 0) {
            this.speakButton.setEnabled(true);
         } else {
            Log.e("DearSteveTAG", "Language is not available.");
         }
      } else {
         Log.e("DearSteveTAG", "Could not initialize TextToSpeech.");
      }

   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      switch(var1.getItemId()) {
      case R.id.settings:
         this.startActivity((new Intent()).setClass(this, Settings.class));
         break;
      case R.id.insult_us:
         this.startActivity((new Intent()).setClass(this, SendForm.class));
      }

      return false;
   }

   public void setRunned() {
      Editor var1 = this.mPrefs.edit();
      var1.putBoolean("firstRun", false);
      var1.commit();
   }
}
