package com.insult.maker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.HashMap;
import com.insult.maker.HttpConnection;

public class SendForm extends Activity {
   private EditText comments_input;
   private ProgressDialog dialog;
   private EditText insult_input;
   private Button ko_button;
   private EditText name_input;
   private Button ok_button;

   private void hide_keyboard() {
      ((InputMethodManager)this.getSystemService("input_method")).hideSoftInputFromWindow(this.name_input.getWindowToken(), 0);
   }

   private void send_button_pressed() {
      this.hide_keyboard();
      if(this.name_input.getText().toString().length() <= 0) {
         this.show_no_steve_message(this.getString(R.string.no_name));
      } else if(this.insult_input.getText().toString().toLowerCase().indexOf("steve") == -1) {
         this.show_no_steve_message(this.getString(R.string.no_steve));
      } else {
         this.send_insult();
      }

   }

   private void show_no_steve_message(String var1) {
      Builder var2 = new Builder(this);
      var2.setMessage(var1).setCancelable(false).setPositiveButton(this.getString(R.string.i_understand), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.dismiss();
         }
      });
      var2.create().show();
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.send_form);
      this.ok_button = (Button)this.findViewById(R.id.send_button);
      this.ko_button = (Button)this.findViewById(R.id.cancel_button);
      this.name_input = (EditText)this.findViewById(R.id.name_input);
      this.insult_input = (EditText)this.findViewById(R.id.insult_input);
      this.comments_input = (EditText)this.findViewById(R.id.comments_input);
      ((TextView)this.findViewById(R.id.send_intro)).setMovementMethod(LinkMovementMethod.getInstance());
      this.ko_button.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            SendForm.this.finish();
         }
      });
      this.ok_button.setOnClickListener(new android.view.View.OnClickListener() {
         public void onClick(View var1) {
            SendForm.this.send_button_pressed();
         }
      });
   }

   protected void onResume() {
      super.onResume();
   }

   public void send_insult() {
      HashMap var1 = new HashMap();
      String var5 = this.name_input.getText().toString();
      String var2 = this.insult_input.getText().toString();
      String var4 = this.comments_input.getText().toString();
      String var3 = this.getResources().getConfiguration().locale.getLanguage();
      var1.put("name", var5);
      var1.put("insult", var2);
      var1.put("comments", var4);
      var1.put("language", var3);

      try {
         Handler var7 = new Handler() {
            public void handleMessage(Message var1) {
               switch(var1.what) {
               case 0:
               default:
                  break;
               case 1:
                  ((Exception)var1.obj).printStackTrace();
                  Toast.makeText(SendForm.this, R.string.sending_ko, 1).show();
                  SendForm.this.dialog.cancel();
                  break;
               case 2:
                  Toast.makeText(SendForm.this, R.string.sending_ok, 1).show();
                  SendForm.this.dialog.cancel();
               }

            }
         };
         this.dialog = ProgressDialog.show(this, "", this.getString(R.string.sending_wait), true);
         (new HttpConnection(var7)).post("http://atizasl.com/insultator/send_insult.php", var1);
      } catch (IOException var6) {
         var6.printStackTrace();
      }

   }
}
