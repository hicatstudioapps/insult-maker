package com.insult.maker;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;
import com.insult.maker.InsultatorMain;

public class Settings extends Activity {
   private AudioManager audio_manager;
   private Spinner spinner;
   private Button test_button;
   private Button tts_button;
   private SeekBar volume_control;
   private int lan;

   private void setTTSlanguage() {
//      if(InsultatorMain.mTts != null) {
//         int var1 = this.spinner.getSelectedItemPosition();
         switch(lan) {
         case 0:
            InsultatorMain.mTts.setLanguage(Locale.ENGLISH);
            break;
         case 1:
            InsultatorMain.mTts.setLanguage(new Locale("spa"));
            break;
         case 2:
            InsultatorMain.mTts.setLanguage(Locale.FRENCH);
            break;
         case 3:
            InsultatorMain.mTts.setLanguage(Locale.GERMAN);
         }

         InsultatorMain.selected_language = lan;
         InsultatorMain.generator.setInsults(lan, InsultatorMain.is_male);
//      }

   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.settings);
      this.spinner = (Spinner)this.findViewById(R.id.language_select);

      ((TextView)findViewById(R.id.volumen)).setTypeface(SpotTheCatApp.faceRR);
      ((TextView)findViewById(R.id.texto)).setTypeface(SpotTheCatApp.faceRR);
      ((TextView)findViewById(R.id.lenguaje)).setTypeface(SpotTheCatApp.faceRR);
      findViewById(R.id.button_es).setOnClickListener(new OnClickListener() {
         @Override
         public void onClick(View v) {
            lan = 1;
            setTTSlanguage();
         }
      });
      ((Button)findViewById(R.id.button_en)).setTypeface(SpotTheCatApp.faceRR);
      ((Button)findViewById(R.id.button_es)).setTypeface(SpotTheCatApp.faceRR);
//      ((Button)findViewById(R.id.button_en)).setTypeface(SpotTheCatApp.faceRR);

      findViewById(R.id.button_en).setOnClickListener(new OnClickListener() {
         @Override
         public void onClick(View v) {
            lan = 0;
            setTTSlanguage();
         }
      });
      this.audio_manager = (AudioManager)this.getSystemService("audio");
      this.volume_control = (SeekBar)this.findViewById(R.id.volume_control);
      this.volume_control.setMax(this.audio_manager.getStreamMaxVolume(3));
      this.volume_control.setProgress(this.audio_manager.getStreamVolume(3));
      this.volume_control.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
         public void onProgressChanged(SeekBar var1, int var2, boolean var3) {
            Settings.this.audio_manager.setStreamVolume(3, var2, 0);
         }

         public void onStartTrackingTouch(SeekBar var1) {
         }

         public void onStopTrackingTouch(SeekBar var1) {
         }
      });
      this.tts_button = (Button)this.findViewById(R.id.tts_button);
      this.tts_button.setTypeface(SpotTheCatApp.faceRR);
      this.tts_button.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            try {
               ComponentName var4 = new ComponentName("com.android.settings", "com.android.settings.TextToSpeechSettings");
               Intent var2 = new Intent();
               var2.addCategory("android.intent.category.LAUNCHER");
               var2.setComponent(var4);
               var2.setFlags(268435456);
               Settings.this.startActivity(var2);
            } catch (Exception var3) {
               Settings.this.startActivityForResult(new Intent("android.settings.SOUND_SETTINGS"), 0);
            }

         }
      });
      this.test_button = (Button)this.findViewById(R.id.test_button);
      this.test_button.setTypeface(SpotTheCatApp.faceRR);
      this.test_button.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            InsultatorMain.say_something_to_Steve("Steve");
         }
      });
      SpotTheCatApp.showInterstitial();
   }
}
