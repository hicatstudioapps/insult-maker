package com.insult.maker;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.message.BasicNameValuePair;
import com.insult.maker.ConnectionManager;

public class HttpConnection implements Runnable {
   private static final int BITMAP = 4;
   private static final int DELETE = 3;
   public static final int DID_ERROR = 1;
   public static final int DID_START = 0;
   public static final int DID_SUCCEED = 2;
   private static final int GET = 0;
   private static final int POST = 1;
   private static final int PUT = 2;
   private String data;
   private Handler handler;
   private HttpClient httpClient;
   private int method;
   private UrlEncodedFormEntity postEntity;
   private String url;

   public HttpConnection() {
      this(new Handler());
   }

   public HttpConnection(Handler var1) {
      this.handler = var1;
   }

   private void processBitmapEntity(HttpEntity var1) throws IOException {
      Bitmap var2 = BitmapFactory.decodeStream((new BufferedHttpEntity(var1)).getContent());
      this.handler.sendMessage(Message.obtain(this.handler, 2, var2));
   }

   private void processEntity(HttpEntity var1) throws IllegalStateException, IOException {
      BufferedReader var2 = new BufferedReader(new InputStreamReader(var1.getContent()));
      String var4 = "";

      while(true) {
         String var3 = var2.readLine();
         if(var3 == null) {
            Message var5 = Message.obtain(this.handler, 2, var4);
            this.handler.sendMessage(var5);
            return;
         }

         var4 = var4 + var3;
      }
   }

   public void bitmap(String var1) {
      this.create(4, var1, (String)null);
   }

   public void create(int var1, String var2, String var3) {
      this.method = var1;
      this.url = var2;
      this.data = var3;
      ConnectionManager.getInstance().push(this);
   }

   public void delete(String var1) {
      this.create(3, var1, (String)null);
   }

   public void get(String var1) {
      this.create(0, var1, (String)null);
   }

   public void post(String var1, Map var2) throws UnsupportedEncodingException {
      if(var2 != null && !var2.isEmpty()) {
         ArrayList var4 = new ArrayList(var2.size());
         Iterator var5 = var2.keySet().iterator();

         while(var5.hasNext()) {
            String var3 = (String)var5.next();
            var4.add(new BasicNameValuePair(var3, (String)var2.get(var3)));
         }

         this.postEntity = new UrlEncodedFormEntity(var4);
      }

      this.create(1, var1, this.data);
   }

   public void put(String var1, String var2) {
      this.create(2, var1, var2);
   }

   public void run() {
      // $FF: Couldn't be decompiled
   }
}
