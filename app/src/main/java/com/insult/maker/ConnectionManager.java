package com.insult.maker;

import java.util.ArrayList;

public class ConnectionManager {
   public static final int MAX_CONNECTIONS = 5;
   private static ConnectionManager instance;
   private ArrayList active = new ArrayList();
   private ArrayList queue = new ArrayList();

   public static ConnectionManager getInstance() {
      if(instance == null) {
         instance = new ConnectionManager();
      }

      return instance;
   }

   private void startNext() {
      if(!this.queue.isEmpty()) {
         Runnable var1 = (Runnable)this.queue.get(0);
         this.queue.remove(0);
         this.active.add(var1);
         (new Thread(var1)).start();
      }

   }

   public void didComplete(Runnable var1) {
      this.active.remove(var1);
      this.startNext();
   }

   public void push(Runnable var1) {
      this.queue.add(var1);
      if(this.active.size() < 5) {
         this.startNext();
      }

   }
}
