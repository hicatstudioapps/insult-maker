package com.insult.maker;

import java.util.Random;
import com.insult.maker.InsultatorMain;
import com.insult.maker.Insultsdb;

public class InsultGenerator {

   private static final Random RANDOM = new Random();
   private String[] insults;
   private int last_id = 0;
   private int num_used;
   private boolean[] used_insults;

   public InsultGenerator() {
      this.setInsults(InsultatorMain.selected_language, InsultatorMain.is_male);
   }

   private int get_unused_index() {
      int var2 = RANDOM.nextInt(this.insults.length);
      int var1 = var2;
      if(this.used_insults.length == this.num_used) {
         this.reset_used();
         var1 = var2;
      }

      while(this.used_insults[var1]) {
         var2 = var1 + 1;
         var1 = var2;
         if(var2 >= this.used_insults.length) {
            var1 = 0;
         }
      }

      ++this.num_used;
      this.used_insults[var1] = true;
      return var1;
   }

   public String get_insult(String var1, int var2) {
      this.last_id = this.get_unused_index();
      String var4 = this.insults[this.last_id];
      String var3 = var4;
      if(var1.length() > 0) {
         var3 = var4.replace("Steve", var1);
      }

      var1 = var3;
      if(var2 == 1) {
         var1 = var3.replace("Steve", "Stif");
      }

      return var1;
   }

   public int get_last_id() {
      return this.last_id;
   }

   public void reset_used() {
      this.used_insults = new boolean[this.insults.length];
      this.num_used = 0;

      for(int var1 = 0; var1 < this.insults.length; ++var1) {
         this.used_insults[var1] = false;
      }

   }

   public void setInsults(int var1, boolean var2) {
      this.insults = Insultsdb.INSULTS_EN;
      switch(var1) {
      case 0:
         this.insults = Insultsdb.INSULTS_EN;
         break;
      case 1:
         if(var2) {
            this.insults = Insultsdb.INSULTS_ES;
         } else {
            this.insults = Insultsdb.INSULTS_ES_FEM;
         }
         break;
      case 2:
         this.insults = Insultsdb.INSULTS_FR;
         break;
      case 3:
         this.insults = Insultsdb.INSULTS_AL;
      }

      this.reset_used();
   }
}
